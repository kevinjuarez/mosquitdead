package com.example.mosquitdead;

import androidx.appcompat.app.AppCompatActivity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import java.util.Random;


public class MainActivity extends AppCompatActivity {
    private ImageButton ib_mosquit;
    AnimationDrawable mosquit_animat;
    Animation volar;
    Random r;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setVariables();
        setListeners();
        startGame();
    }
    public void setVariables()
    {
        ib_mosquit = (ImageButton) findViewById(R.id.ib_mosquit);
        volar = AnimationUtils.loadAnimation(this, R.anim.mosquit_vol);
        mosquit_animat = new AnimationDrawable();
        r = new Random();
    }


    private void setListeners()
    {
        ib_mosquit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materMesquite();
            }
        });
    }


    public void startGame()
    {
        ib_mosquit.setBackgroundResource(R.drawable.mosquit_animat);
        ib_mosquit.setX(r.nextInt(1000));
        ib_mosquit.setY(r.nextInt(600));
        ib_mosquit.startAnimation(volar);
        startAnimation();
    }

    public void startAnimation()
    {
        mosquit_animat = (AnimationDrawable) ib_mosquit.getBackground();
        mosquit_animat.start();
    }

    public void materMesquite()
    {
        ib_mosquit.setBackgroundResource(R.drawable.sang_animat);
        ib_mosquit.clearAnimation();
        startAnimation();

        // Wait 2.sec until start another game to see how mosquito die
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() { startGame(); }
        }, 2000);
    }
}